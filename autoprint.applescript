#!/usr/bin/osascript

-- Return code when user do not want to close it's running chromium instance
property userCanceled : -128
-- State of Google Chrome app
set chromeIsRunning to application "Google Chrome" is running

-- Force use to close Google Chrome if it is running
if chromeIsRunning then
  set question to display dialog "Chrome is runnning, please quit it now" buttons {"Yes", "No"}
  set answer to button returned of question

  if answer is equal to "No" then
    display dialog "Sorry you must quit Google Chrome first..."
    error number userCanceled
  else
    tell application "Google Chrome" to quit
  end if
end if

-- Wait for Google Chrome to finish execution
repeat while chromeIsRunning
  set chromeIsRunning to application "Google Chrome" is running
end repeat

-- Re-open Google Chrome with required flags
do shell script "open -a 'Google Chrome' --args --kiosk-printing https://www.sharypic.com"
