# How it works

Description: use Chromium with two modes enabled.

1. --kiosk, so Chromium is launched on fullscreen
2. --kiosk-printing, so Chromium print modal is skipped

# Build
See: build.sh

```
osacompile \
  -o autoprint.app \
  autoprint.applescript
```

# Deploy

Distributed from AWS s3 ask link to @mfo
